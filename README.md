# Script de Instalare LibreWolf

Acest script automatizează procesul de adăugare a depozitului oficial LibreWolf și instalarea pachetului LibreWolf pe sistemul dumneavoastră. LibreWolf este o furcă concentrată pe confidențialitate a navigatorului Firefox.

## Utilizare

1. Clonați acest depozit sau descărcați scriptul.
2. Asigurați-vă că aveți permisiunile necesare pentru a executa scriptul:
   ```bash
   chmod +x librewolf-installer.sh
   ```
3. Rulați scriptul:
   ```bash
   ./librewolf-installer.sh
   ```

## Instrucțiuni

Scriptul efectuează următorii pași:
- Adaugă depozitul oficial LibreWolf pe sistemul dumneavoastră.
- Instalează pachetul LibreWolf folosind managerul de pachete `dnf`.

## Notă importantă

În timpul procesului de instalare, s-ar putea să fiți întrebat să importați cheia GPG cu amprenta `034F7776EF5E0C613D2F7934D29FBD5F93C0CFC3`. Asigurați-vă că o acceptați pentru a continua cu instalarea.

## Compatibilitate

Acest script este conceput pentru sisteme care utilizează managerul de pachete `dnf`.