#!/bin/bash

# Adăugați depozitul oficial LibreWolf
curl -fsSL https://rpm.librewolf.net/librewolf-repo.repo | pkexec tee /etc/yum.repos.d/librewolf.repo

# Instalați pachetul LibreWolf
sudo dnf install librewolf

# Sau, pe desktopurile Atomic, utilizați:
# rpm-ostree install librewolf

# Acceptați orice prompt care vă cere să importați cheia GPG cu amprenta 034F7776EF5E0C613D2F7934D29FBD5F93C0CFC3.

